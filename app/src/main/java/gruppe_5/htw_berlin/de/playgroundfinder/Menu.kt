package gruppe_5.htw_berlin.de.playgroundfinder

import android.content.Context
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.LinearLayout

/**
 * Class sets showing and hiding animations on linearLayout. Class sets all button actions.
 */
class Menu(menu : LinearLayout) {
  /**
   * To save submitted layout object and sets values for animations.
   */
  private val menuLayout = menu

  /**
   * To save showing animation.
   */
  private val showing = TranslateAnimation(-1450F , 0F , 0F ,0F)

  /**
   * To save hiding animation.
   */
  private val hiding = TranslateAnimation(0F , -1450F , 0F ,0F)

  /**
   * Initializes showing and hiding animations for the menu.
   */
  init {
    showing.duration = 2000
    showing.fillAfter = true

    hiding.duration = 2000
    hiding.fillAfter = true

    showing.setAnimationListener(object : Animation.AnimationListener {
      override fun onAnimationRepeat(p0: Animation?) {}

      override fun onAnimationEnd(p0: Animation?) {
        menu.translationX = 0F
      }

      override fun onAnimationStart(p0: Animation?) {
        menu.visibility = View.VISIBLE
      }
    })

    hiding.setAnimationListener(object : Animation.AnimationListener{
      override fun onAnimationRepeat(p0: Animation?) {}

      override fun onAnimationEnd(p0: Animation?) {
        menu.visibility = View.INVISIBLE
      }

      override fun onAnimationStart(p0: Animation?) {}
    })
  }

  /**
   * Shows menu layout with an animation.
   */
  fun showMenu() {
    menuLayout.startAnimation(showing)
  }

  /**
   * Hides menu layout with an animation.
   */
  fun hideMenu() {
    menuLayout.startAnimation(hiding)
  }

  /**
   * Opens search view by on click event.
   * @param context - To show menu on submitted context.
   */
  fun openSearch(context : Context) {
    val intent = Intent(context , SearchActivity::class.java)
    context.startActivity(intent)
  }

  /**
   * Returns a dialog window with about information.
   * @param context - Dialog will connect to submitted context.
   */
  fun openAboutDialog(context : Context) : AlertDialog.Builder {
    val aboutDialog = AlertDialog.Builder(context)
    aboutDialog.setTitle(R.string.about_dialog_title)
    aboutDialog.setView(R.layout.about_content)
    aboutDialog.setNeutralButton(R.string.about_dialog_close) {
      dialog , _ ->
      dialog.cancel()
    }

    return aboutDialog
  }

  /**
   * Closes app with status 0.
   */
  fun closeApp() {
    System.exit(0)
  }
}
