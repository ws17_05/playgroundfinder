package gruppe_5.htw_berlin.de.playgroundfinder

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.text.Editable
import android.text.TextWatcher
import android.widget.*

/**
 * Activity to handle search view. Class sets on click actions to menu buttons.
 * Also class handle address searching and it opens the open street map by geocode.
 */
class SearchActivity : AppCompatActivity() {
  /**
   * Fix Request Code for requestPermissionLocation()
   */
  private val ACCESS_FINE_LOCATION = 1002

  /**
   * Sets all listener on relevant menu buttons. Also method declares all variables to show
   * finding address by user input.
   * @param savedInstanceState - Instance of a bundle to get saved values.
   */
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_search)


    findViewById<LinearLayout>(R.id.menuFragment).removeView(findViewById(R.id.searchLayoutByMenu))
    val menu = MenuHandler(findViewById(R.id.menuFragment))
    findViewById<TableLayout>(R.id.contentSearchActivity).setOnTouchListener(menu.getListenerToCloseOpeningMenu())
    findViewById<ImageButton>(R.id.menu).setOnClickListener { menu.setVisibility() }
    findViewById<Button>(R.id.aboutMenu).setOnClickListener{ menu.onClickOpenAboutDialog(this)}
    findViewById<Button>(R.id.closeByMenu).setOnClickListener { menu.onClickCloseApp() }

    setAddressButtonListener(R.id.address1)
    setAddressButtonListener(R.id.address2)
    setAddressButtonListener(R.id.address3)
    val addressLayout = findViewById<LinearLayout>(R.id.addressLayout)
    val addressInput = findViewById<EditText>(R.id.placeInput)
    val addressField1 = findViewById<Button>(R.id.address1)
    val addressField2 = findViewById<Button>(R.id.address2)
    val addressField3 = findViewById<Button>(R.id.address3)
    val field1Params = addressField1.layoutParams
    val field2Params = addressField2.layoutParams
    val field3Params = addressField3.layoutParams
    val layoutParams = addressLayout.layoutParams
    val layoutHeightBeginning = addressLayout.height
    val extraHeight = 250
    val buttonHeight = 150

    addressInput.addTextChangedListener(object : TextWatcher {
      /**
       * If text changes method looks for real addresses with same name as input text.
       * Method returns maximum three addresses and sets address name as button text. Furthermore it
       * shows buttons with address name and hide buttons if found addresses less then the maximum.
       * @param p0 - Input text from user.
       */
      override fun afterTextChanged(p0: Editable?) {
        Thread().run {
          val addresses = StreetAddressHandler.getListOfAddresses(p0.toString() , 3 , this@SearchActivity)

          when {
            addresses.size == 1 -> {
              field1Params.height = buttonHeight
              field2Params.height = 0
              field3Params.height = 0
              layoutParams.height = layoutHeightBeginning + buttonHeight + extraHeight
              addressField1.layoutParams = field1Params
              addressField2.layoutParams = field2Params
              addressField3.layoutParams = field3Params
              addressLayout.layoutParams = layoutParams
              addressField1.text = StreetAddressHandler.getFormattedAddressString(addresses[0])
            }
            addresses.size == 2 -> {
              field1Params.height = buttonHeight
              field2Params.height = buttonHeight
              field3Params.height = 0
              layoutParams.height = layoutHeightBeginning + (2 * buttonHeight) + extraHeight
              addressField1.layoutParams = field1Params
              addressField2.layoutParams = field2Params
              addressField3.layoutParams = field3Params
              addressLayout.layoutParams = layoutParams
              addressField1.text = StreetAddressHandler.getFormattedAddressString(addresses[0])
              addressField2.text = StreetAddressHandler.getFormattedAddressString(addresses[1])
            }
            addresses.size == 3 -> {
              field1Params.height = buttonHeight
              field2Params.height = buttonHeight
              field3Params.height = buttonHeight
              layoutParams.height = layoutHeightBeginning + (3 * buttonHeight) + extraHeight
              addressField1.layoutParams = field1Params
              addressField2.layoutParams = field2Params
              addressField3.layoutParams = field3Params
              addressLayout.layoutParams = layoutParams
              addressField1.text = StreetAddressHandler.getFormattedAddressString(addresses[0])
              addressField2.text = StreetAddressHandler.getFormattedAddressString(addresses[1])
              addressField3.text = StreetAddressHandler.getFormattedAddressString(addresses[2])
            }
          }
        }
      }

      /**
       * Non relevant method for overriding action.
       */
      override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

      /**
       * Non relevant method for overriding action.
       */
      override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    })
  }

  /**
   * Gets GPS coordinates to open map if app has all permissions.
   * @param v - Needs to open method by XML command
   */
  fun onClickGetGPSPosition(v : View) {
    if (hasPermissionLocation()) {
      val gps = GPSListener(this)
      if (gps.canGetLocation()) {
        gps.stopUsingGPS()
        openMapActivity(gps.getLatitude() , gps.getLongitude())
      }
      else {
        gps.stopUsingGPS()
        Toast.makeText(applicationContext, getString(R.string.search_error), Toast.LENGTH_LONG).show()
      }
    }
    else {
      Toast.makeText(applicationContext , getString(R.string.search_no_permission) , Toast.LENGTH_LONG).show()
      requestPermissionLocation()
    }
  }

  /**
   * Closes search activity.
   * @param v - Needs to open method by XML command
   */
  fun onClickReturnToPrevActivity(v : View) {
    finish()
  }

  /**
   * Opens the map activity by given geocode (latitude , longitude).
   * The geocode will put into a Bundle object and send to next activity.
   * @param latitude - To open map with a position marker
   * @param longitude - To open map with a position marker
   */
  private fun openMapActivity(latitude : Double , longitude : Double) {
    val intent = Intent(this , MapActivity::class.java)
    val coordinates = Bundle()

    if (latitude == 0.0 && longitude == 0.0) {
      Toast.makeText(applicationContext, getString(R.string.search_error), Toast.LENGTH_LONG).show()
    }
    else {
      coordinates.putDouble("latitude" , latitude)
      coordinates.putDouble("longitude" , longitude)
      intent.putExtras(coordinates)
      startActivity(intent)
    }
  }

  /**
   * Sets an onClickListener to button by given id. The Listener will open the map with geocode
   * created by given button text.
   * @param id - Button-ID
   */
  private fun setAddressButtonListener(id : Int) {
    val button = findViewById<Button>(id)
    button.setOnClickListener {
      if(hasPermissionLocation()) {
        val geocode = StreetAddressHandler.getGeoCodeFromAddress(button.text.toString() , this)
        openMapActivity(geocode["latitude"] as Double , geocode["longitude"] as Double)
      }
      else {
        Toast.makeText(applicationContext , getString(R.string.search_no_permission) , Toast.LENGTH_LONG).show()
        requestPermissionLocation()
      }

    }
  }

  /**
   * Checks if location permission exists.
   * Don't check in older (pre-marshmallow) versions which don't support requests on run time.
   * (However: these older version ask on install so the permission exists for sure.)
   * @return - Returns true if permission exists
   */
  private fun hasPermissionLocation(): Boolean {
    return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
      true
    } else {
      (ActivityCompat.checkSelfPermission(this,
              Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
    }
  }

  /**
   * Asks the user for location permission
   */
  private fun requestPermissionLocation() {
    ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), ACCESS_FINE_LOCATION)
  }
}