package gruppe_5.htw_berlin.de.playgroundfinder

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.LinearLayout

/**
 * Handles starting view actions including menu and searching.
 */
class MainActivity : AppCompatActivity() {
  /**
   * At start method sets all touch and click listener.
   */
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    val menu = MenuHandler(findViewById(R.id.menuFragment))
    findViewById<LinearLayout>(R.id.contentMainActivity).setOnTouchListener(menu.getListenerToCloseOpeningMenu())
    findViewById<ImageButton>(R.id.menu).setOnClickListener { menu.setVisibility() }
    findViewById<Button>(R.id.closeByMenu).setOnClickListener { menu.onClickCloseApp() }
    findViewById<Button>(R.id.searchByMenu).setOnClickListener { menu.onClickOpenSearchByMenu(this) }
    findViewById<Button>(R.id.aboutMenu).setOnClickListener{ menu.onClickOpenAboutDialog(this)}
  }

  /**
   * Opens search view.
   * @param v - Needs to open method by XML command
   */
  fun openSearch(v : View) {
    val intent = Intent(this , SearchActivity::class.java)
    startActivity(intent)
  }
}
