package gruppe_5.htw_berlin.de.playgroundfinder

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Creates menu view as a fragment.
 */
class MenuFragment : Fragment() {
  /**
   * @param inflater - Instantiates a layout XML file into a view.
   * @param container - Views gets the layout by inflater
   * @param savedInstanceState - Instance from submitted view.
   * @return Returns views with integrated layout xml.
   */
  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
    return inflater.inflate(R.layout.menu_layout , container , false)
  }

}