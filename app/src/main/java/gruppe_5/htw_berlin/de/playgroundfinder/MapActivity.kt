package gruppe_5.htw_berlin.de.playgroundfinder

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import org.osmdroid.bonuspack.kml.KmlDocument
import org.osmdroid.bonuspack.location.OverpassAPIProvider
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import android.support.v4.content.ContextCompat
import android.widget.Button
import android.widget.ImageButton
import org.osmdroid.util.BoundingBox
import org.osmdroid.views.overlay.*

/**
 * Activity to handle map view. Draws open street map (OSM) on screen.
 * Marks several points on map like search position and playgrounds.
 */
class MapActivity : AppCompatActivity() {
  /**
   * Gets geocode from Bundle object and draws open street map on screen.
   * Sets all listener on relevant menu buttons.
   * @param savedInstanceState - Instance of a bundle to get saved values (geocode).
   */
  public override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val ctx = applicationContext

    Configuration.getInstance().userAgentValue = packageName
    Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx))

    setContentView(R.layout.activity_map)

    val menu = MenuHandler(findViewById(R.id.menuFragment))
    findViewById<MapView>(R.id.map).setOnTouchListener(menu.getListenerToCloseOpeningMenu())
    findViewById<ImageButton>(R.id.menu).setOnClickListener { menu.setVisibility() }
    findViewById<Button>(R.id.closeByMenu).setOnClickListener { menu.onClickCloseApp() }
    findViewById<Button>(R.id.searchByMenu).setOnClickListener { menu.onClickCloseActivity(this) }
    findViewById<Button>(R.id.aboutMenu).setOnClickListener{ menu.onClickOpenAboutDialog(this)}

    val map = findViewById<View>(R.id.map) as MapView
    val bundle = intent.extras
    val viewPoint = GeoPoint(bundle.getDouble("latitude"), bundle.getDouble("longitude"))

    setupMapSettings(map, viewPoint)
    Thread({ markPlaygrounds(map, viewPoint) }).start()
    markSearchPosition(map, viewPoint, ctx)
  }

  /**
   * Setup of settings: zoom, zoom-level, multi-touch, tile source (Mapnik) and center of map
   * @param map - Map to setup settings for
   * @param viewPoint - Point to set center to
   */
  private fun setupMapSettings(map: MapView, viewPoint: GeoPoint) {
    map.setTileSource(TileSourceFactory.MAPNIK)
    map.setBuiltInZoomControls(true)
    map.setMultiTouchControls(true)
    val mapController = map.controller
    mapController.setZoom(15)
    mapController.setCenter(viewPoint)
  }

  /**
   * Marks playgrounds with help of osmdroid bonuspack (KML flavor).
   * Needs to be started in extra thread.
   * @param map - Map to mark playgrounds on
   * @param viewPoint - Point to find nearby playgrounds for
   */
  private fun markPlaygrounds(map: MapView, viewPoint: GeoPoint) {
    val overpassProvider = OverpassAPIProvider()
    val boundingBox = calculateBoundingBox(viewPoint, 0.015)
    val url = overpassProvider.urlForTagSearchKml(
            getString(R.string.map_leisure_tag), boundingBox, 150, 60)
    val kmlDocument = KmlDocument()
    if (overpassProvider.addInKmlFolder(kmlDocument.mKmlRoot, url)) {
      val kmlOverlay = kmlDocument.mKmlRoot.buildOverlay(
              map, null, null, kmlDocument) as FolderOverlay
      map.overlays.add(kmlOverlay)
      map.invalidate()
    }
  }

  /**
   * Calculates a Bounding Box of reasonable size for a given point
   * @param geoPoint - Center point
   * @param distance - Distance to center point
   * @return - Returns area around given point
   */
  fun calculateBoundingBox(geoPoint: GeoPoint, distance: Double): BoundingBox {
    return BoundingBox(geoPoint.latitude + distance, geoPoint.longitude + distance * 1.5,
            geoPoint.latitude - distance, geoPoint.longitude - distance * 1.5)
  }

  /**
   * Marks search position with marker icon (person)
   * @param map - Map to mark search position on
   * @param viewPoint - Point of search position
   * @param ctx - - Needs a context for marker icon and overlay.
   */
  private fun markSearchPosition(map: MapView, viewPoint: GeoPoint, ctx: Context) {
    val itemsList = ArrayList<OverlayItem>()
    val item = OverlayItem(getString(R.string.map_search_pos_headline),
            getString(R.string.map_search_pos_text), viewPoint)
    val markerIcon = ContextCompat.getDrawable(ctx, R.drawable.person)
    item.setMarker(markerIcon)
    itemsList.add(item)

    val mOverlay = ItemizedOverlayWithFocus<OverlayItem>(itemsList,
            object : ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {
              /**
               * Non relevant method for overriding action.
               */
              override fun onItemSingleTapUp(index: Int, item: OverlayItem): Boolean {
                return true
              }

              /**
               * Non relevant method for overriding action.
               */
              override fun onItemLongPress(index: Int, item: OverlayItem): Boolean {
                return false
              }
            }, ctx)

    mOverlay.setFocusItemsOnTap(true)
    map.overlays.add(mOverlay)
  }

  /**
   * Closes search activity.
   * @param v - Needs to open method by XML command
   */
  fun onClickReturnToPrevActivity(v : View) {
    finish()
  }

  /**
   * Method for overriding action.
   */
  public override fun onResume() {
    super.onResume()
    Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this))
  }

}
