package gruppe_5.htw_berlin.de.playgroundfinder

import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.IBinder
import android.util.Log
import java.io.IOException
/**
 * Class handle geocode or address by input values. You can get geocode from address or list of
 * suitable addresses by input string
 *
 */
class StreetAddressHandler : Service() {
  companion object {
    /**
     * Method search for latitude and longitude by given address as string.
     * @param address - Address to search for latitude and longitude.
     * @param context - Needs a context for Geocoder object.
     * @throws IllegalArgumentException - Throws an exception, if Geocoder gets a wrong Argument.
     * @return - Returns a map, which is filled with geocode:
     * - Key: "latitude" -> Double value
     * - Key: "longitude" -> Double value
     */
    @Throws(IllegalArgumentException::class)
    fun getGeoCodeFromAddress(address: String, context: Context): HashMap<String, Double> {
      val geocode = hashMapOf("latitude" to 0.0, "longitude" to 0.0)
      try {
        val addressResult = Geocoder(context).getFromLocationName(address, 1)
        if (!addressResult.isEmpty()) {
          geocode["latitude"] = addressResult[0].latitude
          geocode["longitude"] = addressResult[0].longitude
        }
      } catch (e: IOException) {
        Log.d("IOException","Ignore ERROR from Geocoder.getFromLocationName() " +
                "in getGeoCodeFromAddress(). Hopefully a temporary error. " + e.message)
      }
      return geocode
    }

    /**
     * Method creates an ArrayList with Address objects by given address. The address must not be
     * written correct because the Geocoder figure out the correct address name hopefully.
     * @param addressSearch - Address to figure out correct and complete address name.
     * @param returningAmount - Amount of founding addresses from Geocoder.
     * @param context - Needs a context fpr Geocoder object.
     * @throws IllegalArgumentException - Throws an exception, if Geocoder gets a wrong Argument.
     * @return - Returns an ArrayList filled with founding addresses
     */
    @Throws(IllegalArgumentException::class)
    fun getListOfAddresses(addressSearch: String, returningAmount: Int, context: Context): List<Address> {
      var adresses = emptyList<Address>()
      try {
        adresses = Geocoder(context).getFromLocationName(addressSearch, returningAmount)
      } catch (e: IOException) {
        Log.d("IOException","Ignore ERROR from Geocoder.getFromLocationName() " +
                "in getListOfAddresses(). Hopefully a temporary error. " + e.message)
      }
      return adresses
    }

    /**
     * Method returns address name with postcode and city name or country name, if available.
     * @param address - Address object to get address information.
     * @return - Returns a formatted address information string.
     */
    fun getFormattedAddressString(address : Address) : String {
      return address.getAddressLine(0) + " , " + address.getAddressLine(1)
    }
  }

  /**
   * Non relevant method for overriding action.
   */
  override fun onBind(p0: Intent?): IBinder? { return null }
}