package gruppe_5.htw_berlin.de.playgroundfinder

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout

/**
 * Object handle all menu actions to show and hide the menu. Also it handles all click events from
 * including menu buttons.
 */
class MenuHandler(menuLayout : LinearLayout) {
  /**
   * Creates a menu object to handle it.
   */
  private val menu = Menu(menuLayout)

  /**
   * Saves status of menu. If menu is open the variables is true otherwise false.
   */
  private var showMenu = false

  /**
   * Hide or show menu.
   */
  fun setVisibility() {
    showMenu = if(!showMenu) {
      menu.showMenu()
      true
    } else {
      menu.hideMenu()
      false
    }
  }

  /**
   * Gets an on touch listener to recognize click on a view during menu is opening.
   * @return - Returns an on touch listener.
   */
  fun getListenerToCloseOpeningMenu() : View.OnTouchListener {
    return View.OnTouchListener { view, event ->
      if(event.action == MotionEvent.ACTION_DOWN && showMenu) {
        setVisibility()
        return@OnTouchListener true
      }
      view.performClick()
      return@OnTouchListener false
    }
  }

  /**
   * Opens search view by on click event.
   * @param context - To show menu on submitted context.
   */
  fun onClickOpenSearchByMenu(context : Context) {
    menu.openSearch(context)
  }

  /**
   * Method closes submitted view.
   * @param view - To finish current view.
   */
  fun onClickCloseActivity(view : AppCompatActivity) {
    view.finish()
  }

  /**
   * Opens about dialog by  on click event.
   * @param context - To show dialog on this context.
   */
  fun onClickOpenAboutDialog(context : Context) {
    menu.openAboutDialog(context).show()
  }

  /**
   * Close app by on click event.
   */
  fun onClickCloseApp() {
    menu.closeApp()
  }
}