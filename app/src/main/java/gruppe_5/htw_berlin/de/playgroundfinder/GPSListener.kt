package gruppe_5.htw_berlin.de.playgroundfinder

import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.provider.Settings
import android.os.Bundle
import android.os.IBinder
import android.util.Log

/**
 * Class looks for geocode by GPS tracking. GPSListener needs permission either network or
 * direct gps
 */
class GPSListener(context: Context) : Service(), LocationListener {
  /**
   * Saves current context to set system service.
   */
  private var mContext : Context = context
  /**
   * Sets minimum distance to update the geocode.
   */
  private val MIN_DISTANCE_CHANGE_FOR_UPDATES : Float = 100.0f
  /**
   * Sets minimum time to update current location.
   */
  private val MIN_TIME_BW_UPDATES : Long = 1000 * 60 * 1
  /**
   * Creates location manager to get geocode.
   */
  private var locManager : LocationManager = mContext.getSystemService(LOCATION_SERVICE) as LocationManager
  /**
   * Saves gps permission if it is enabled or otherwise.
   */
  private var isGPSEnabled : Boolean = false
  /**
   * Saves network permission if it is enabled or otherwise.
   */
  private var isNetworkEnabled : Boolean = false
  /**
   * Saves status of access right for getting location.
   */
  private var canGetLocation : Boolean = false
  /**
   * Saves loaded location with location manager.
   */
  private var location : Location? = null
  /**
   * Saves latitude from location object.
   */
  private var latitude : Double = 0.0
  /**
   * Saves longitude from location object.
   */
  private var longitude : Double = 0.0

  /**
   * Class needs a constructor to set this class as service into AndroidManifest.
   */
  constructor() : this(context = SearchActivity().applicationContext)

  /**
   * Initializes all information to get geocode from current location.
   */
  init {
    getLocation()
  }

  /**
   * Gets location geocode by using GPS.
   * @throws SecurityException - Throws an exception if security isn`t assured by permission denied.
   */
  @Throws(SecurityException::class)
  private fun getLocation() {
    isGPSEnabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    isNetworkEnabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

    if(!isGPSEnabled || !isNetworkEnabled) {

    }
    else {
      this.canGetLocation = true

      if(!isNetworkEnabled) {
        locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER ,
                MIN_TIME_BW_UPDATES ,
                MIN_DISTANCE_CHANGE_FOR_UPDATES ,
                this)
        location = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)

        if(location != null) {
          latitude = location!!.latitude
          longitude = location!!.longitude
        }
      }
      else if(isGPSEnabled) {
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER ,
                MIN_TIME_BW_UPDATES ,
                MIN_DISTANCE_CHANGE_FOR_UPDATES ,
                this)
        location = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)

        if(location != null) {
          latitude = location!!.latitude
          longitude = location!!.longitude
        }
      }
    }
  }

  /**
   * Stops GPS tracking.
   */
  fun stopUsingGPS() {
    locManager.removeUpdates(this)
  }

  /**
   * Gets latitude from current location.
   * @return - Returns latitude.
   */
  fun getLatitude() : Double {
    return latitude
  }

  /**
   * Gets longitude from current location.
   * @return - Returns longitude.
   */
  fun getLongitude() : Double {
    return longitude
  }

  /**
   * Returns accessibility to get geocode from current location.
   * @return - Return true if location maanger have access right otherwise false.
   */
  fun canGetLocation() : Boolean {
      return canGetLocation
  }

  /**
   * Non relevant method for overriding action.
   */
  override fun onBind(p0: Intent?): IBinder? {
    return null
  }

  /**
   * Non relevant method for overriding action.
   */
  override fun onLocationChanged(p0: Location?) {}

  /**
   * Non relevant method for overriding action.
   */
  override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}

  /**
   * Non relevant method for overriding action.
   */
  override fun onProviderEnabled(p0: String?) {}

  /**
   * Non relevant method for overriding action.
   */
  override fun onProviderDisabled(p0: String?) {}
}