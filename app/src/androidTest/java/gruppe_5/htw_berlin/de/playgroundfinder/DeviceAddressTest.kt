package gruppe_5.htw_berlin.de.playgroundfinder

import android.location.Address
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Device address test, which will execute on an Android device.
 * Checks StreetAddressHandler returning objects. Furthermore it checks returning geocode values.
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class DeviceAddressTest {
  /**
   * Initializes testing context.
   */
  private val appContext = InstrumentationRegistry.getTargetContext()

  /**
   * Checks package name, provided by Android Studio
   */
  @Test
  fun useAppContext() {
    val appContext = InstrumentationRegistry.getTargetContext()
    assertEquals("gruppe_5.htw_berlin.de.playgroundfinder", appContext.packageName)
  }

  /**
   * Checks correct object by returning address list.
   */
  @Test
  fun checkReturningAddressListObject() {
    assertEquals(ArrayList::class , StreetAddressHandler.getListOfAddresses("berlin" , 3 , appContext)::class)
  }

  /**
   * Checks address object from  returning array list.
   */
  @Test
  fun checkReturningAddressObject() {
    val address = StreetAddressHandler.getListOfAddresses("berlin" , 1 , appContext)

    assertEquals(Address::class , address[0]::class)
  }

  /**
   * Checks correct geocode from the address "Cecilienstrasse 242 , 12619 Berlin"
   * Correct geocode: latitude: 52.5225 ; longitude: 13.59189
   */
  @Test
  fun checkCorrectGeoCode() {
    val geocode = StreetAddressHandler.getGeoCodeFromAddress("Cecilienstrasse 242 , 12619 Berlin" , appContext)
    assertEquals(52.5225 , geocode["latitude"])
    assertEquals(13.59189 , geocode["longitude"])
  }
}
