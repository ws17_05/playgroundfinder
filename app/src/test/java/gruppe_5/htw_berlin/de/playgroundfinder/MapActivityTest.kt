package gruppe_5.htw_berlin.de.playgroundfinder

import org.junit.Test

import org.osmdroid.util.BoundingBox

import org.osmdroid.util.GeoPoint


class MapActivityTest {

    private val testMapActivity = MapActivity()
    private val geoPoint = GeoPoint(52.5223199, 13.4130839)
    private val resultBoundingBox = BoundingBox(52.5523199, 13.4580839,
                                                52.4923199, 13.3680839)

    /**
     * Happy Path
     */
    @Test
    fun calculateBoundingBox() {
        val calculatedBoundingBox = testMapActivity.calculateBoundingBox(this.geoPoint, 0.03)

        assert((resultBoundingBox.latNorth == calculatedBoundingBox.latNorth)
                && (resultBoundingBox.latSouth == calculatedBoundingBox.latSouth)
                && (resultBoundingBox.lonEast == calculatedBoundingBox.lonEast)
                && (resultBoundingBox.lonWest == calculatedBoundingBox.lonWest))
    }

}