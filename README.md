##Was ist der PlaygroundFinder?##
* Der "PlaygroundFinder" soll es Eltern (oder anderen Erziehungsberechtigten) ermöglichen, schnell und bequem auch in ihnen unbekannten Orten den nächstgelegenen Spielplatz für ihr Kind zu finden.

![alt text](https://bytebucket.org/ws17_05/playgroundfinder/raw/1a3226e6d1d45fee67fe31f9bae9a386cdf2e19d/screenshots/01_start-window.jpg?token=da962222d73052de5767207bb226754b20d66526)
![alt text](https://bytebucket.org/ws17_05/playgroundfinder/raw/1a3226e6d1d45fee67fe31f9bae9a386cdf2e19d/screenshots/05_street-result-info-window.jpg?token=df2c10b7c3e001886af7cc21c81db83dfae709a3)

##Wer braucht die App?##
* Eltern müssen oft ähnlich spontan wie ihre Kinder sein – Spielplätze werden nicht selten passend
  zur Stimmung des Kindes und zur aktuellen Wetterlage angesteuert. Beides sind schwer zu
  kalkulierende Größen.
  
##Welche Moeglichkeiten bietet mir die Software?##
* Die App PlaygroundFinder zeigt entweder nach der manuellen Angabe eines Standortes oder nach
  der automatischen Ermittlung des aktuellen Standortes per GPS die nächstgelegenen Spielplätze auf
  dem Screen des Android-Smartphones an.
* Hierzu wird Kartenmaterial von OpenStreet-Map genutzt, sowie enthaltene öffentliche Daten, die öffentliche und private Spielplätze einschließen.

##Wie kann ich den PlaygroundFinder benutzen?##
* Zum Importieren des Projekts geben Sie folgenden Befehl ein:  
```
git clone https://bitbucket.org/ws17_05/playgroundfinder.git
```
* Empfehlenswert ist es, die SDK ab Version 27 (mindestens 23) installiert zu haben.

## Wie kann ich die App auf mein Android-Handy installieren?##
* Im Ordner /install befindet sich eine Installationsdatei im .apk Format. Diese kann einfach auf das Handy kopiert und schließlich geöffnet werden. 
* Danach wird die App installiert.

##Wie kann ich zum Projekt beitragen?##
* Als Entwickler sind wir offen fuer neue Ideen und Verbesserungen. Schreiben Sie uns einfach einen neuen Issue.

##Informationen zu den Autoren##
* Uni-Gruppenarbeit: Lumak, KenTacki